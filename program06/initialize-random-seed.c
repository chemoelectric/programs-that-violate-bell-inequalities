/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to
 * initialize-random-seed.c. This work is published from: United
 * States.  See http://creativecommons.org/publicdomain/zero/1.0/ for
 * details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include "initialize-random-seed.h"

void
initialize_random_seed (void)
{
  /* Cheap and dirty initial seeding. I really have no idea how good
     or bad this is. */

  FILE *f;
  size_t n;
  unsigned int seed;

  f = fopen ("/dev/urandom", "rb");
  if (f != NULL)
    {
      do
        n = fread (&seed, sizeof (seed), 1, f);
      while (n != 0 && seed == 0);
      fclose (f);
    }
  else
    {
      const time_t t = time (NULL);
      const pid_t pid = getpid ();
      const unsigned int i1 = (unsigned int) (t % UINT_MAX);
      const unsigned int i2 = (unsigned int) (pid % UINT_MAX);

      seed = i1 ^ i2;
    }

  if (seed == 0)
    seed = 12345;
  srandom (seed);
}
