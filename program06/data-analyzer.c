/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to
 * data-analyzer.c. This work is published from: United States.  See
 * http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include "vect.h"

const char *program_name = NULL;

static void
print_usage_and_exit (void)
{
  printf ("Usage: %s ANGLE-A1 ANGLE-A2 ANGLE-B1 ANGLE-B2\n", program_name);
  exit (1);
}

static void
check_index (unsigned int dimension, unsigned int i)
{
  if (dimension <= i)
    {
      fprintf (stderr,
               "%s: corrupted input -- index %u out of range [0,%u]\n",
               program_name, i, dimension - 1);
      exit (1);
    }
}

/* Signs by quadrant (1 to 4). The 0-elements are unused. */
static const int cos_signs[] = { 9999, +1, -1, -1, +1 };
static const int sin_signs[] = { 9999, +1, +1, -1, -1 };

typedef struct
{
  unsigned int source_angle;
  double a_angle_predicted;
  double b_angle_predicted;
  int a_cos_sign;
  int a_sin_sign;
  int b_cos_sign;
  int b_sin_sign;
  double denominator;
  double a1_detections;         /* (+) channel */
  double a2_detections;         /* (-) channel */
  double b1_detections;         /* (+) channel */
  double b2_detections;         /* (-) channel */
} per_settings_totals;

typedef per_settings_totals totals_array[2][2][2];

static double
cos_sign (per_settings_totals t, unsigned int station)
{
  return (station == 0) ? t.a_cos_sign : t.b_cos_sign;
}

static double
sin_sign (per_settings_totals t, unsigned int station)
{
  return (station == 0) ? t.a_sin_sign : t.b_sin_sign;
}

static double
channel1_detections (per_settings_totals t, unsigned int station)
{
  return (station == 0) ? t.a1_detections : t.b1_detections;
}

static double
channel2_detections (per_settings_totals t, unsigned int station)
{
  return (station == 0) ? t.a2_detections : t.b2_detections;
}

static void
initialize_totals (double angles[2][2], totals_array totals)
{
  unsigned int source_angle;
  unsigned int a_angle;
  unsigned int b_angle;
  per_settings_totals t;
  int a_quadrant;
  int b_quadrant;

  for (source_angle = 0; source_angle < 2; source_angle++)
    for (a_angle = 0; a_angle < 2; a_angle++)
      for (b_angle = 0; b_angle < 2; b_angle++)
        {
          t.source_angle = source_angle;

          t.a_angle_predicted = angles[0][a_angle];
          t.b_angle_predicted = angles[1][b_angle];

          a_quadrant = vect_quadrant (make_vect (1, t.a_angle_predicted));
          b_quadrant = vect_quadrant (make_vect (1, t.b_angle_predicted));
          t.a_cos_sign = cos_signs[a_quadrant];
          t.a_sin_sign = sin_signs[a_quadrant];
          t.b_cos_sign = cos_signs[b_quadrant];
          t.b_sin_sign = sin_signs[b_quadrant];

          /* initial accumulator settings */
          t.denominator = 0;
          t.a1_detections = 0;
          t.a2_detections = 0;
          t.b1_detections = 0;
          t.b2_detections = 0;

          totals[source_angle][a_angle][b_angle] = t;
        }
}

static void
update_totals (unsigned int source_angle, unsigned int a_angle,
               unsigned int b_angle, double a1, double a2, double b1,
               double b2, totals_array totals)
{
  per_settings_totals *t;

  t = &totals[source_angle][a_angle][b_angle];
  t->denominator += 1;
  t->a1_detections += a1;
  t->a2_detections += a2;
  t->b1_detections += b1;
  t->b2_detections += b2;
}

static double
relative_frequency (totals_array totals, unsigned int station)
{
  double s1;
  double s2;
  unsigned int source_angle;
  unsigned int a_angle;
  unsigned int b_angle;
  per_settings_totals t;

  s1 = 0;
  s2 = 0;
  for (source_angle = 0; source_angle < 2; source_angle++)
    for (a_angle = 0; a_angle < 2; a_angle++)
      for (b_angle = 0; b_angle < 2; b_angle++)
        {
          t = totals[source_angle][a_angle][b_angle];
          s1 += channel1_detections (t, station);
          s2 += channel2_detections (t, station);
        }
  return (s1 + s2 == 0) ? 0 : (s1 / (s1 + s2));
}

static vect
per_settings_vector_estimate (per_settings_totals t, int station)
{
  double cos_estimate;
  double sin_estimate;

  const double denom = (t.denominator == 0) ? 1 : t.denominator;
  const double detections1 = channel1_detections (t, station);
  const double detections2 = channel2_detections (t, station);

  if (t.source_angle == station)
    {
      /* source polarization = 0 degrees */
      cos_estimate = cos_sign (t, station) * sqrt (detections1 / denom);
      sin_estimate = sin_sign (t, station) * sqrt (detections2 / denom);
    }
  else
    {
      /* source polarization = 90 degrees */
      cos_estimate = cos_sign (t, station) * sqrt (detections2 / denom);
      sin_estimate = sin_sign (t, station) * sqrt (detections1 / denom);
    }

  return rect_to_vect (cos_estimate, sin_estimate);
}

static void
estimate_vectors (totals_array totals,
                  vect a_estimated_vectors[2][2],
                  vect b_estimated_vectors[2][2])
{
  unsigned int a_angle;
  unsigned int b_angle;

  for (a_angle = 0; a_angle < 2; a_angle++)
    for (b_angle = 0; b_angle < 2; b_angle++)
      {
        /* source polarization = 0 degrees */
        const per_settings_totals t0 = totals[0][a_angle][b_angle];
        const double N0 = t0.denominator;

        /* source polarization = 90 degrees */
        const per_settings_totals t1 = totals[1][a_angle][b_angle];
        const double N1 = t1.denominator;

        /* Station A with source polarization = 0 degrees */
        const vect va0 = per_settings_vector_estimate (t0, 0);

        /* Station A with source polarization = 90 degrees */
        const vect va1 = per_settings_vector_estimate (t1, 0);

        /* Station B with source polarization = 0 degrees */
        const vect vb0 = per_settings_vector_estimate (t0, 1);

        /* Station B with source polarization = 90 degrees */
        const vect vb1 = per_settings_vector_estimate (t1, 1);

        const double N =
          (N0 + N1 < 100 * DBL_EPSILON) ? DBL_EPSILON : (N0 + N1);

        /* Weighted averages */
        a_estimated_vectors[a_angle][b_angle] =
          vect_sum (vect_scalar_product (va0, N0 / N),
                    vect_scalar_product (va1, N1 / N));
        b_estimated_vectors[a_angle][b_angle] =
          vect_sum (vect_scalar_product (vb0, N0 / N),
                    vect_scalar_product (vb1, N1 / N));
      }
}

static void
compute_correlations (vect a_estimated_vectors[2][2],
                      vect b_estimated_vectors[2][2],
                      double correlations[2][2])
{
  unsigned int a_angle;
  unsigned int b_angle;

  for (a_angle = 0; a_angle < 2; a_angle++)
    for (b_angle = 0; b_angle < 2; b_angle++)
      {
        const double p =
          vect_dot_product (a_estimated_vectors[a_angle][b_angle],
                            b_estimated_vectors[a_angle][b_angle]);
        correlations[a_angle][b_angle] = 2 * p * p - 1;
      }
}

int
main (int argc, char **argv)
{
  unsigned int source_angle;
  unsigned int a_angle;
  unsigned int b_angle;
  double a1;
  double a2;
  double b1;
  double b2;
  int retval;
  totals_array totals;
  double angles[2][2];
  vect a_estimated_vectors[2][2];
  vect b_estimated_vectors[2][2];
  double correlations[2][2];
  double chsh_contrast;

  program_name = argv[0];

  if (argc != 5)
    print_usage_and_exit ();
  angles[0][0] = atof (argv[1]);
  angles[0][1] = atof (argv[2]);
  angles[1][0] = atof (argv[3]);
  angles[1][1] = atof (argv[4]);

  initialize_totals (angles, totals);

  retval = scanf ("%u %u %u %lg %lg %lg %lg",
                  &source_angle, &a_angle, &b_angle, &a1, &a2, &b1, &b2);
  while (retval != EOF)
    {
      unsigned int i;
      unsigned int j;

      check_index (2, source_angle);
      check_index (2, a_angle);
      check_index (2, b_angle);

      update_totals (source_angle, a_angle, b_angle, a1, a2, b1, b2, totals);
      estimate_vectors (totals, a_estimated_vectors, b_estimated_vectors);
      compute_correlations (a_estimated_vectors, b_estimated_vectors,
                            correlations);
      chsh_contrast =
        correlations[0][0] - correlations[0][1] + correlations[1][0] +
        correlations[1][1];

      printf ("%lf %lf", relative_frequency (totals, 0),
              relative_frequency (totals, 1));
      for (i = 0; i < 2; i++)
        for (j = 0; j < 2; j++)
          printf (" %lf %lf %lf",
                  a_estimated_vectors[i][j].theta,
                  b_estimated_vectors[i][j].theta, correlations[i][j]);
      printf (" %lf\n", chsh_contrast);

      retval = scanf ("%u %u %u %lg %lg %lg %lg",
                      &source_angle, &a_angle, &b_angle, &a1, &a2, &b1, &b2);
    }

  return 0;
}
