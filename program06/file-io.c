/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to
 * file-io.c. This work is published from: United States.
 * See http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

#include <stdlib.h>
#include "file-io.h"

static void
print_file_unopenable_and_exit (const char *path, const char *mode)
{
  printf ("cannot open `%s' in mode `%s'\n", path, mode);
  exit (1);
}

FILE *
fopen_or_exit (const char *path, const char *mode)
{
  FILE *f = fopen (path, mode);
  if (f == NULL)
    print_file_unopenable_and_exit (path, mode);
  return f;
}
