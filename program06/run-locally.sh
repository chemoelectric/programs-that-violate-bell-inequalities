#!/bin/sh
#
# Run with FIFOs locally (that is, not over the network).
#
# The default is to run 10000 trials with conventional Bell test
# angles that give maximum contrast.
#
# Public domain dedication
# ------------------------
#
# To the extent possible under law, Barry Schwartz has waived all
# copyright and related or neighboring rights to run-locally. This
# work is published from: United States.  See
# http://creativecommons.org/publicdomain/zero/1.0/ for details.
#

num_trials=${1:-10000}
angle_a1=${2:-0}
angle_a2=${3:-45}
angle_b1=${4:-22.5}
angle_b2=${5:-67.5}

# Clean up at exit.
trap '
rm -f A A1 AC1 A2 AC2
rm -f B B1 BC1 B2 BC2
rm -f BUFFER
rm -f SOURCE-ANGLES A-ANGLES B-ANGLES
' 0

mkfifo A                        # The pulse.
mkfifo A1                       # After-splitting pulse 1.
mkfifo AC1                      # Photodetector current 1.
mkfifo A2                       # After-splitting pulse 2.
mkfifo AC2                      # Photodetector current 2.

mkfifo B                        # The pulse.
mkfifo B1                       # After-splitting pulse 1.
mkfifo BC1                      # Photodetector current 1.
mkfifo B2                       # After-splitting pulse 2.
mkfifo BC2                      # Photodetector current 2.

mkfifo BUFFER                   # Temporary storage for the results.

# Logs.
mkfifo SOURCE-ANGLES
mkfifo A-ANGLES
mkfifo B-ANGLES

./polarizing-beam-splitter ${angle_a1} ${angle_a2} A A1 A2 A-ANGLES &
./photodetector A1 AC1 &
./photodetector A2 AC2 &

./polarizing-beam-splitter ${angle_b1} ${angle_b2} B B1 B2 B-ANGLES &
./photodetector B1 BC1 &
./photodetector B2 BC2 &

#
# After each trial, print:
#   * the relative frequency of detections at A1
#   * the relative frequency of detections at B1
#   * the four correlation coefficients
#   * the CHSH contrast
#
(./data-synchronizer SOURCE-ANGLES A-ANGLES B-ANGLES AC1 AC2 BC1 BC2 | \
    ./data-analyzer ${angle_a1} ${angle_a2} ${angle_b1} ${angle_b2} | \
    cut -d ' ' -f 1,2,5,8,11,14,15 > BUFFER) &

./light-source ${num_trials} A B SOURCE-ANGLES

cat BUFFER

exit 0
