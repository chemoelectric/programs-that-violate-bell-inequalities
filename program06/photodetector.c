/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to
 * photodetector.c. This work is published from: United States.  See
 * http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

#include <stdlib.h>
#include "vect.h"
#include "file-io.h"
#include "initialize-random-seed.h"

const char *program_name = NULL;

static void
print_usage_and_exit (void)
{
  printf ("Usage: %s INPUT OUTPUT\n", program_name);
  exit (1);
}

static void
photodetector (vect pulse, double *current)
{
  if (random () / ((double) RAND_MAX + 1) <= pulse.a * pulse.a)
    *current = 1;
  else
    *current = 0;
}

int
main (int argc, char **argv)
{
  vect pulse;
  double current;
  const char *input;
  const char *output;
  FILE *fi;
  FILE *fo;
  int retval;

  program_name = argv[0];

  initialize_random_seed ();

  if (argc != 3)
    print_usage_and_exit ();
  input = argv[1];
  output = argv[2];

  fi = fopen_or_exit (input, "r");
  fo = fopen_or_exit (output, "w");

  retval = fscanf (fi, "%lg %lg", &pulse.a, &pulse.theta);
  while (retval != EOF)
    {
      photodetector (pulse, &current);
      fprintf (fo, "%lg\n", current);
      retval = fscanf (fi, "%lg %lg", &pulse.a, &pulse.theta);
    }

  fclose (fi);
  fclose (fo);

  return 0;
}
