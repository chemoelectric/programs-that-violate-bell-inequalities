/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to vect.c. This work is
 * published from: United States.  See
 * http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

#include "vect.h"

static double
modulo (double a, double p)
{
  return (a - floor (a / p) * p);
}

vect
make_vect (double a, double theta)
{
  vect v;

  v.a = a;
  v.theta = modulo (theta, 360);
  return v;
}

vect
rect_to_vect (double x, double y)
{
  vect v;

  v.a = sqrt (x * x + y * y);
  v.theta = modulo ((180 * M_1_PI) * atan2 (y, x), 360);
  return v;
}

void
vect_to_rect (vect v, double *x, double *y)
{
  *x = v.a * cos ((M_PI / 180) * v.theta);
  *y = v.a * sin ((M_PI / 180) * v.theta);
}

static int
angle_quadrant (double theta)
{
  int quadrant;

  theta = modulo (theta, 360);
  if (theta < 90)
    quadrant = 1;
  else if (theta < 180)
    quadrant = 2;
  else if (theta < 270)
    quadrant = 3;
  else
    quadrant = 4;

  return quadrant;
}

int
vect_quadrant (vect v)
{
  return angle_quadrant (v.theta);
}

vect
rotate_vect (vect v, double theta)
{
  return make_vect (v.a, v.theta + theta);
}

vect
vect_scalar_product (vect v, double a)
{
  return make_vect (v.a * a, v.theta);
}

double
vect_dot_product (vect a, vect b)
{
  return a.a * b.a * cos ((M_PI / 180) * (a.theta - b.theta));
}

vect
vect_sum (vect a, vect b)
{
  double xa;
  double ya;
  double xb;
  double yb;

  vect_to_rect (a, &xa, &ya);
  vect_to_rect (b, &xb, &yb);

  return rect_to_vect (xa + xb, ya + yb);
}
