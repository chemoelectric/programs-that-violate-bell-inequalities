/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to
 * polarizing-beam-splitter.c. This work is published from: United
 * States.  See http://creativecommons.org/publicdomain/zero/1.0/ for
 * details.
 */

#include <stdlib.h>
#include "vect.h"
#include "file-io.h"
#include "initialize-random-seed.h"

const char *program_name = NULL;

static void
print_usage_and_exit (void)
{
  printf ("Usage: %s ANGLE1 ANGLE2 INPUT OUTPUT1 OUTPUT2 ANGLE-LOG\n",
          program_name);
  exit (1);
}

static void
polarizing_beam_splitter (double angle, vect pulse,
                          vect *output_pulse1, vect *output_pulse2)
{
  *output_pulse1 =
    make_vect (pulse.a * cos ((M_PI / 180) * (angle - pulse.theta)), angle);
  *output_pulse2 =
    make_vect (pulse.a * cos ((M_PI / 180) * (angle + 90 - pulse.theta)),
               angle + 90);
}

int
main (int argc, char **argv)
{
  double angles[2];
  vect pulse;
  vect output_pulse1;
  vect output_pulse2;
  const char *input;
  const char *output1;
  const char *output2;
  const char *angle_log;
  FILE *fi;
  FILE *fo1;
  FILE *fo2;
  FILE *fal;
  int retval;

  program_name = argv[0];

  initialize_random_seed ();

  if (argc != 7)
    print_usage_and_exit ();
  angles[0] = atof (argv[1]);
  angles[1] = atof (argv[2]);
  input = argv[3];
  output1 = argv[4];
  output2 = argv[5];
  angle_log = argv[6];

  fi = fopen_or_exit (input, "r");
  fo1 = fopen_or_exit (output1, "w");
  fo2 = fopen_or_exit (output2, "w");
  fal = fopen_or_exit (angle_log, "w");

  retval = fscanf (fi, "%lf %lf", &pulse.a, &pulse.theta);
  while (retval != EOF)
    {
      const unsigned int k = random () % 2;
      polarizing_beam_splitter (angles[k], pulse, &output_pulse1,
                                &output_pulse2);
      fprintf (fo1, "%8.6lf %10.6lf\n", output_pulse1.a, output_pulse1.theta);
      fprintf (fo2, "%8.6lf %10.6lf\n", output_pulse2.a, output_pulse2.theta);
      fprintf (fal, "%d\n", k);
      retval = fscanf (fi, "%lf %lf", &pulse.a, &pulse.theta);
    }

  fclose (fi);
  fclose (fo1);
  fclose (fo2);
  fclose (fal);

  return 0;
}
