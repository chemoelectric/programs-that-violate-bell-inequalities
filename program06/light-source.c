/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to light-source.c. This
 * work is published from: United States.  See
 * http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "vect.h"
#include "file-io.h"
#include "initialize-random-seed.h"

const char *program_name = NULL;

static void
print_usage_and_exit (void)
{
  printf ("Usage: %s N-TRIALS OUTPUT-A OUTPUT-B ANGLE-LOG\n", program_name);
  exit (1);
}

int
main (int argc, char **argv)
{
  long int n_trials;
  long int i;
  const char *output_a;
  const char *output_b;
  const char *angle_log;
  FILE *fa;
  FILE *fb;
  FILE *fal;

  const vect v0 = make_vect (1, 0);
  const vect v1 = make_vect (1, 90);

  program_name = argv[0];

  initialize_random_seed ();

  if (argc != 5)
    print_usage_and_exit ();
  n_trials = atol (argv[1]);
  output_a = argv[2];
  output_b = argv[3];
  angle_log = argv[4];

  fa = fopen_or_exit (output_a, "w");
  fb = fopen_or_exit (output_b, "w");
  fal = fopen_or_exit (angle_log, "w");

  for (i = 0; i < n_trials; i++)
    {
      if (random () % 2 == 0)
        {
          fprintf (fa, "%8.6lf %10.6lf\n", v0.a, v0.theta);
          fprintf (fb, "%8.6lf %10.6lf\n", v1.a, v1.theta);
          fprintf (fal, "0\n");
        }
      else
        {
          fprintf (fa, "%8.6lf %10.6lf\n", v1.a, v1.theta);
          fprintf (fb, "%8.6lf %10.6lf\n", v0.a, v0.theta);
          fprintf (fal, "1\n");
        }
    }

  fclose (fa);
  fclose (fb);
  fclose (fal);

  return 0;
}
