#!/bin/sh
#
# Run the simulation over a network, using OpenSSH.
#
# The default is to run 10000 trials with conventional Bell test
# angles that give maximum contrast.
#
# Public domain dedication
# ------------------------
#
# To the extent possible under law, Barry Schwartz has waived all
# copyright and related or neighboring rights to run-remotely. This
# work is published from: United States.  See
# http://creativecommons.org/publicdomain/zero/1.0/ for details.
#

alice=${1:-localhost:`pwd`}
bob=${2:-localhost:`pwd`}
num_trials=${3:-10000}
angle_a1=${4:-0}
angle_a2=${5:-45}
angle_b1=${6:-22.5}
angle_b2=${7:-67.5}

ssh=ssh                         # OpenSSH.

alice_host=`echo "${alice}" | sed -e 's/:.*//'`
alice_dir=`echo "${alice}" | sed -e 's/[^:]*://'`
bob_host=`echo "${bob}" | sed -e 's/:.*//'`
bob_dir=`echo "${bob}" | sed -e 's/[^:]*://'`

# Clean up at exit.
trap '
${ssh} "${alice_host}" rm -f "${alice_dir}/A" "${alice_dir}/A1" "${alice_dir}/AC1" \
    "${alice_dir}/A2" "${alice_dir}/AC2" "${alice_dir}/A-ANGLES"
${ssh} "${bob_host}" rm -f "${bob_dir}/B" "${bob_dir}/B1" "${bob_dir}/BC1" \
    "${bob_dir}/B2" "${bob_dir}/BC2" "${bob_dir}/B-ANGLES"
rm -f rA rA1 rAC1 rA2 rAC2 rA-ANGLES
rm -f rB rB1 rBC1 rB2 rBC2 rB-ANGLES
rm -f SOURCE-ANGLES
' 0

./light-source ${num_trials} rA rB SOURCE-ANGLES

${ssh} ${alice_host} "cat > ${alice_dir}/A" < rA
${ssh} ${alice_host} "${alice_dir}/polarizing-beam-splitter ${angle_a1} ${angle_a2} ${alice_dir}/A ${alice_dir}/A1 ${alice_dir}/A2 ${alice_dir}/A-ANGLES"
${ssh} ${alice_host} "${alice_dir}/photodetector ${alice_dir}/A1 ${alice_dir}/AC1"
${ssh} ${alice_host} "${alice_dir}/photodetector ${alice_dir}/A2 ${alice_dir}/AC2"
${ssh} ${alice_host} "cat ${alice_dir}/AC1" > rAC1
${ssh} ${alice_host} "cat ${alice_dir}/AC2" > rAC2
${ssh} ${alice_host} "cat ${alice_dir}/A-ANGLES" > rA-ANGLES

${ssh} ${bob_host} "cat > ${bob_dir}/B" < rB
${ssh} ${bob_host} "${bob_dir}/polarizing-beam-splitter ${angle_b1} ${angle_b2} ${bob_dir}/B ${bob_dir}/B1 ${bob_dir}/B2 ${bob_dir}/B-ANGLES"
${ssh} ${bob_host} "${bob_dir}/photodetector ${bob_dir}/B1 ${bob_dir}/BC1"
${ssh} ${bob_host} "${bob_dir}/photodetector ${bob_dir}/B2 ${bob_dir}/BC2"
${ssh} ${bob_host} "cat ${bob_dir}/BC1" > rBC1
${ssh} ${bob_host} "cat ${bob_dir}/BC2" > rBC2
${ssh} ${bob_host} "cat ${bob_dir}/B-ANGLES" > rB-ANGLES

#
# After each trial, print:
#   * the relative frequency of detections at A1
#   * the relative frequency of detections at B1
#   * the four correlation coefficients
#   * the CHSH contrast
#
./data-synchronizer SOURCE-ANGLES rA-ANGLES rB-ANGLES rAC1 rAC2 rBC1 rBC2 | \
    ./data-analyzer ${angle_a1} ${angle_a2} ${angle_b1} ${angle_b2} | \
    cut -d ' ' -f 1,2,5,8,11,14,15

exit 0
