/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to vect.h. This work is
 * published from: United States.  See
 * http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

#ifndef _VECT_H
#define _VECT_H

#include <math.h>

typedef struct
{
  double a;                     /* magnitude */
  double theta;                 /* angle in degrees */
} vect;

vect make_vect (double a, double theta);
vect rect_to_vect (double x, double y);
void vect_to_rect (vect v, double *x, double *y);
int vect_quadrant (vect v);
vect rotate_vect (vect v, double theta);
vect vect_scalar_product (vect v, double a);
double vect_dot_product (vect a, vect b);
vect vect_sum (vect a, vect b);

#endif /* _VECT_H */
