/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to
 * data-synchronizer.c. This work is published from: United States.  See
 * http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

/*
 * Merge the data streams.
 *
 * On POSIX one could also simply use the paste(1) command.
 */

#include <stdlib.h>
#include "file-io.h"

const char *program_name = NULL;

static void
print_usage_and_exit (void)
{
  printf
    ("Usage: %s SOURCE-ANGLES A-ANGLES B-ANGLES A1-CURRENT A2-CURRENT B1-CURRENT B2-CURRENT\n",
     program_name);
  exit (1);
}

static void
fscanf_uint_or_exit (FILE *f, unsigned int *value)
{
  int retval;

  retval = fscanf (f, "%u", value);
  if (retval == EOF)
    {
      printf ("read error or premature end of file\n");
      exit (1);
    }
}

static void
fscanf_double_or_exit (FILE *f, double *value)
{
  int retval;

  retval = fscanf (f, "%lg", value);
  if (retval == EOF)
    {
      printf ("read error or premature end of file\n");
      exit (1);
    }
}

static void
read_data (FILE *f_source_angles, FILE *f_a_angles, FILE *f_b_angles,
           FILE *f_a1_current, FILE *f_a2_current,
           FILE *f_b1_current, FILE *f_b2_current,
           unsigned int *source_angle, unsigned int *a_angle,
           unsigned int *b_angle, double *a1, double *a2, double *b1,
           double *b2, int *eof)
{
  int retval;
  unsigned int value;

  retval = fscanf (f_source_angles, "%u", &value);
  if (retval == EOF)
    {
      *eof = 1;
    }
  else
    {
      *eof = 0;
      *source_angle = value;
      fscanf_uint_or_exit (f_a_angles, a_angle);
      fscanf_uint_or_exit (f_b_angles, b_angle);
      fscanf_double_or_exit (f_a1_current, a1);
      fscanf_double_or_exit (f_a2_current, a2);
      fscanf_double_or_exit (f_b1_current, b1);
      fscanf_double_or_exit (f_b2_current, b2);
    }
}

int
main (int argc, char **argv)
{
  const char *source_angles;
  const char *a_angles;
  const char *b_angles;
  const char *a1_current;
  const char *a2_current;
  const char *b1_current;
  const char *b2_current;
  FILE *f_source_angles;
  FILE *f_a_angles;
  FILE *f_b_angles;
  FILE *f_a1_current;
  FILE *f_a2_current;
  FILE *f_b1_current;
  FILE *f_b2_current;
  unsigned int source_angle;
  unsigned int a_angle;
  unsigned int b_angle;
  double a1;
  double a2;
  double b1;
  double b2;
  int eof;

  program_name = argv[0];

  if (argc != 8)
    print_usage_and_exit ();
  source_angles = argv[1];
  a_angles = argv[2];
  b_angles = argv[3];
  a1_current = argv[4];
  a2_current = argv[5];
  b1_current = argv[6];
  b2_current = argv[7];

  f_source_angles = fopen_or_exit (source_angles, "r");
  f_a_angles = fopen_or_exit (a_angles, "r");
  f_b_angles = fopen_or_exit (b_angles, "r");
  f_a1_current = fopen_or_exit (a1_current, "r");
  f_a2_current = fopen_or_exit (a2_current, "r");
  f_b1_current = fopen_or_exit (b1_current, "r");
  f_b2_current = fopen_or_exit (b2_current, "r");

  do
    {
      read_data (f_source_angles, f_a_angles, f_b_angles,
                 f_a1_current, f_a2_current, f_b1_current, f_b2_current,
                 &source_angle, &a_angle, &b_angle, &a1, &a2, &b1, &b2, &eof);
      if (!eof)
        printf ("%u %u %u %lg %lg %lg %lg\n", source_angle, a_angle,
                b_angle, a1, a2, b1, b2);
    }
  while (!eof);

  fclose (f_source_angles);
  fclose (f_a_angles);
  fclose (f_b_angles);
  fclose (f_a1_current);
  fclose (f_a2_current);
  fclose (f_b1_current);
  fclose (f_b2_current);

  return 0;
}
