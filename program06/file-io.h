/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to file-io.h. This work
 * is published from: United States.  See
 * http://creativecommons.org/publicdomain/zero/1.0/ for details.
 */

#ifndef _FILE_IO_H
#define _FILE_IO_H

#include <stdio.h>

FILE *fopen_or_exit (const char *path, const char *mode);

#endif /* _FILE_IO_H */
