/* -*- coding: utf-8; eval: (c-set-style "gnu"); indent-tabs-mode: nil; -*- */
/*
 * Public domain dedication
 * ------------------------
 *
 * To the extent possible under law, Barry Schwartz has waived all
 * copyright and related or neighboring rights to
 * initialize-random-seed.h. This work is published from: United
 * States.  See http://creativecommons.org/publicdomain/zero/1.0/ for
 * details.
 */

#ifndef _INITIALIZE_RANDOM_SEED_H
#define _INITIALIZE_RANDOM_SEED_H

void initialize_random_seed (void);

#endif /* _INITIALIZE_RANDOM_SEED_H */
